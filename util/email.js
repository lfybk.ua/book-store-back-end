const nodemailer = require("nodemailer");
const { email, admin } = require("../settings");
const text = require("./email-text");

const transporter = nodemailer.createTransport(email)

module.exports = async function sendMessage(data) {
  await transporter.sendMail({
    from: admin.from,
    to: data.email,
    subject: admin.subject,
    html: text(data),
  })

  await transporter.sendMail({
    from: admin.from,
    to: admin.email,
    subject: admin.subject,
    html: text(data),
  })
}