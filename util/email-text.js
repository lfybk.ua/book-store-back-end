

module.exports = ({ firstName: firstName, lastName, phone, email, city, postOffice, pay, delivery, text, data }) => {

  let parameters = "";
  let sum = 0;

  for (let item of Object.values(data)) {
    sum += item.cost * item.counter;
    parameters += `
      <br/>
      <p>Назва: ${item.name}</p>
      <p>Ціна: ${item.cost}грн</p>
      <p>Кількість: ${item.counter}</p>
      <p>isbn: ${item.isbn}</p>
      <br/>
    `
  }

  const str = `
    <div>
      <h2>Деталі замовлення</h2>
      <p>Им'я: ${firstName}</p>
      <p>Прізвище: ${lastName}</p>
      <p>Номер телефону: ${phone}</p>
      <p>еmail: ${email}</p>
      <p>Місто: ${city}</p>
      <p>Відділення/Почтомат: ${postOffice}</p>
      <p>Оплата: ${pay === "card" ? "на карту" : "наложний платіж"}</p>
      <p>Доставка: ${delivery === "department" ? "відділення" : "почтомат"}</p>
      ${text ? `<p>Побажання: ${text}</p>` : ""}
      <br/>
      <h2>Замовлення</h2>
      ${parameters}
      <b>Всього до сплати: ${sum}грн</b> 
      <br/>
      <h2>Для корегування замовлення зателефонуйте за номером: 0734175094</h2>
    </div>
  `

  return str;
}
