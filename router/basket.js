const basketController = require("../controller/basket");

const express = require('express');
const router = express.Router();

router.post('/', basketController.setDelivery);

module.exports = router;
 