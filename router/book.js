const bookController = require("../controller/book")

const express = require('express');
const router = express.Router();

router.get('/', bookController.getBook);

module.exports = router;
