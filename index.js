const express = require('express');
const cors = require('cors');

const book = require("./router/book");

const cluster = require("cluster");
const basket = require('./router/basket');

const addWorker = () => {
  const worker = cluster.fork();

  worker.on('exit', () => {
    console.log(`worker died! Pid ${worker?.process?.pid || worker.pid}`);

    addWorker()
  })
}

if (cluster.isPrimary) {
  addWorker()
} else {
  const app = express();

  const port = process.env.PORT || 5050;

  app.use(cors())
  app.use(express.json());
  app.use(express.urlencoded({
    extended: true
  }));

  app.use('/api/book', book)
  app.use('/api/basket', basket)

  app.use('**', (req, res) => {
    res.redirect('/')
  })

  app.listen(port, (req, res) => {
    console.log(`Example app listening on port ${port}`)
  })
}
