const mysql = require("mysql2");
const { bd } = require("../settings");

const addPull = () => {
  const pull = mysql.createPool(bd);
  return () => pull
}

module.exports = addPull();