const addPull = require("./connect");


class BookModel {
  constructor() {
    this.pull = addPull();
  }

  async getBook() {
    return this.pull.promise().query("SELECT * FROM book");
  }

  async getGenre() {
    return this.pull.promise().query("SELECT * FROM genre");
  }

  async getGenreList() {
    return this.pull.promise().query("SELECT * FROM genre_list");
  }
}

module.exports = BookModel;
