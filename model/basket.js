const addPull = require("./connect");


class BasketModel {
  constructor() {
    this.pull = addPull();
  }

  async set({ firstName: firstName, lastName, phone, email, city, postOffice, text, pay, delivery, data }) {
    return this.pull.promise().query(
      `INSERT INTO delivery (\`id\`, \`city\`, \`delivery\`, \`email\`, \`firstName\`, \`lastName\`, \`pay\`, \`phone\`, \`postOffice\`, \`text\`, \`data\`) VALUES (NULL, '${city}', '${delivery}', '${email}', '${firstName}', '${lastName}', '${pay}', '${phone}', '${postOffice}', '${text}', '${JSON.stringify(data)}');`
    );
  }
}

module.exports = BasketModel;
