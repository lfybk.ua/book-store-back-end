const model = new (require("../model/basket"))();

const sendMail = require("../util/email")


module.exports = {
  async setDelivery(req, res) {
    try {
      await sendMail(req.body)
      await model.set(req.body)

      res.json({
        status: "ok"
      });
    } catch ({ error = "Server error", status = 500, type = "server" }) {
      res.status(status);
      res.json({
        error: error,
        type: type
      })
    }
  },
};