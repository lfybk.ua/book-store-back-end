const model = new (require("../model/book"))();

module.exports = {
  async getBook(req, res) {
    const [book, bookFields] = await model.getBook()
    const [genre, genreFields] = await model.getGenre()
    const [genreList, genreListFields] = await model.getGenreList()


    res.json({
      book: book,
      genre: genre,
      genreList: genreList,
    });
  },
};